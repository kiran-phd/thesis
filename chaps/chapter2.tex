%!TEX root = ../report.tex
\chapter{\texorpdfstring{Analytic \(e^+e^- \rightarrow \text{hadrons}\) calculation}{Analytic electrons to hadrons calculation}} % (fold)
\label{chap:e+e-2hadrons}
The purpose of this section is to make clearer the physics outlined in the previous chapter with an explicit example: the analytic calculation of the process \(e^+e^-\rightarrow \text{hadrons}\) at NLO. Following the QCD factorisation theorem, this calculation is constructed such that the perturbative and non-perturbative physics factorise. The cross section can then be written as a convolution between the \(e^+e^-\rightarrow q\bar{q}\) cross sections and functions that capture the non-perturbative physics. These functions are known as fragmentation functions and in a similar way to PDF's are universal.

In the below calculations it shall be assumed fermions are massless and that we are working in the centre of mass frame.

\section{Total inclusive cross section} \label{sec: tot inc xs}
First suppose we wish to calculate the total inclusive cross section. The Feynman diagrams that contribute at NLO are shown in figure \ref{fig: feyn ee2quarks}. First consider the real-emission matrix element. This will be given by the sum of figures \ref{fig: real1 ee2hadr} and \ref{fig: real2 ee2hadr}. The Feynman rules introduced in section \ref{sec:qcd_feynman_rules} can be used to evaluate these diagrams, giving the following matrix element:

\begin{align}\label{eq: ee me}
    i\mathcal{M} &= i\frac{e^2}{Q^2}t^a\bar{v}(p_b)\gamma^\mu u(p_a) \bar{u}(p_1)S^{\mu\alpha}v(p_2)\epsilon^*_{\alpha}, \\
    S^{\mu\alpha} &= -ig\left[\gamma^{\alpha}\frac{i}{\slashed{p}_1+\slashed{p}_3}\gamma^{\mu}-\gamma^{\mu}\frac{i}{\slashed{p}_2+\slashed{p}_3}\gamma^{\alpha} \right].
 \end{align} 

\noindent The difference in sign between the two diagrams comes from the difference in the quark and antiquark's direction of momentum relative to the propagator in each graph. 

\begin{figure}[t]
    \begin{subfigure}{.5\textwidth}
        \centering
        \input{input/ee2hadr/born}
        \caption{Born level (leading-order)}
        \label{fig: born ee2hadr}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
        \centering
        \input{input/ee2hadr/virt}
        \caption{Virtual}
        \label{fig: virt ee2hadr}
    \end{subfigure}

    \vspace{1cm}
    \begin{subfigure}{.5\textwidth}
        \centering
        \input{input/ee2hadr/qbarreal}
        \caption{Real emisison (off the antiquark)}
        \label{fig: real1 ee2hadr}
    \end{subfigure}
    \begin{subfigure}{.5\textwidth}
        \centering
        \input{input/ee2hadr/qreal}
        \caption{Real emission (off the quark)}
        \label{fig: real2 ee2hadr}
    \end{subfigure}
    \caption{The diagrams contributing at \(\mathcal{O}(\alpha_S)\) to \(e^+e^-\rightarrow q\bar{q}\).}
    \label{fig: feyn ee2quarks}
\end{figure}

Using equation (\ref{eq: i2f xs}) a $2\rightarrow3$ cross section can be explicitly written as:

\begin{equation}\label{eq: xs}
\begin{split}
\dd{\sigma_R}=\frac{1}{\abs{\vb{v_A}-\vb{v_B}}}\frac{1}{2E_A}\frac{1}{2E_B}(2\pi)^4\delta^{(4)}(p_a+p_b-p_1-p_2-p_3)\overline{\abs{\mathcal{M}}}^2 \\ \times \frac{\dd[3]{p_1}}{(2\pi)^32E_1}\frac{\dd[3]{p_2}}{(2\pi)^32E_2}\frac{\dd[3]{p_3}}{(2\pi)^32E_3},
\end{split}
\end{equation}

\noindent where the indices $A$ and $B$ denote the two initial state particles. The bar over $\mathcal{M}$ denotes averaging over initial state spins and summing over final state spins, colours and polarisation (of the gluon).

In equation (\ref{eq: xs}) there are nine degrees of freedom from the final state momentum elements, minus four from the delta function, giving five in total. Three of these will serve as Euler angles, specifying the absolute orientation of the system. The cross section's dependence on these angles is weak compared to the other degrees of freedom and so it is typical to average over them. Hence the integration over phase space can actually be reduced to an integration over two variables. A natural choice would be, for example, the energies of the two quarks. These however are not Lorentz invariant and so it is easier to use the following quantities:

\begin{equation}
    x_i = \frac{p_i\cdot p}{Q^2} = \frac{2E_i}{Q},
\end{equation}

\noindent where the final equality relies on working in the centre of mass frame. Here, $p$ is the total momentum $p_1+p_2+p_3$ . After working through some tedious Dirac algebra, equation (\ref{eq: ee me}) can be squared, simplified and rewritten in terms of \(x_1\) and \(x_2\). The phase-space factors in equation (\ref{eq: xs}) can be handled in a similar way allowing us to rewrite the equation as:

\begin{equation}\label{eq: 4DSigma}
    \dd{\sigma}_R=\sigma_0\frac{\alpha_S}{2\pi}C_F\frac{x_1^2+x_2^2}{(1-x_1)(1-x_2)}\dd{x_1}\dd{x_2}.
\end{equation}

\noindent Here, $C_F$ is the quadratic Casimir for the fundamental representation and comes from squaring the factor of $t_a$ in equation (\ref{eq: ee me}) and summing over colours:

\begin{equation}
    \sum_{ij}\sum_at^a_{ik}t^a_{kj}=\sum_{ij}C_F\delta_{ij}=C_FN_c,
\end{equation}

\noindent $N_c$ is the number of colours. The $\sigma_0$ in equation (\ref{eq: 4DSigma}) is the Born level cross section $\frac{4\pi\alpha^2_{em}}{3Q^2}N_ce_q^4$. In what follows $N_c = 3$ will be used. Note that the singular poles in this equation are now at the points \(x_1\) and \(x_2 =1\). Using conservation of momentum \(x_1\), for example, can be written as \(1-x_2\frac{E_3}{Q}(1-\cos\theta_{23})\). Hence we see that these limits still correspond to the gluon going soft or collinear. 

Were we evaluating this cross section using Monte-Carlo techniques at this point we would introduce a subtraction term to equation (\ref{eq: 4DSigma}) and proceed to sample phase-space points. This will be discussed in detail in section \ref{sec:dipole_subtraction}. However, when performing analytic calculations there are other, more illuminating regularisation methods. The most common one is dimensional regularisation where the divergence is regularised by assuming $d$ dimensions of spacetime rather than 4 (still with only one time dimension) \cite{tHooft:1972tcz}. The result of the integration will still be singular at $d=4$ but not after combining with the virtual cross section. 

To enact this regularisation method equation (\ref{eq: 4DSigma}) must be recalculated but now taking

\begin{equation}
g_{\mu\nu}=(1,-1,-1,\dots ,-1), \qquad g_{\mu\nu}g^{\mu\nu}=d.
\end{equation}

\noindent We must also write $\dd[3]{p_i}$ in equation (\ref{eq: xs}) as

\begin{equation}
\dd[d-1]{p_i}=\abs{\vb{p_i}}^{d-2}\dd{\abs{\vb{p_i}}}\dd{\Omega_i}_{d-1}.
\end{equation}

\noindent Here $\dd{\Omega_d}$ is the elemental solid angle of a $d$-dimensional sphere and upon integration can be written as

\begin{equation}
\int \dd{\Omega}_d=\frac{2\pi^{\frac{d}{2}}}{\Gamma(\frac{d}{2})},
\end{equation}


\noindent where $\Gamma(z)=\int_0^{\infty}x^{z-1} e^{-x}\dd{x}$ is a generalisation of the factorial function to non-integer values. 

It is also helpful to introduce an arbitrary scale $\mu$ to keep $g$ dimensionless. The action, $\mathcal{S}=\int \mathcal{L}\dd[d]{x}$, must remain dimensionless in any dimension and, given that in natural units length is inverse proportional to energy, the units of the Lagrangian will be eV to the power of $[\mathcal{L}]=d$. Recall that the QCD Lagrangian can be written as:

\begin{equation}
\mathcal{L}=\left(\partial_{\mu}A^a_{\nu}-\partial_{\nu}A^a_{\mu}+gf_{abc}A^b_{\mu}A^c_{\nu}\right)^2+\overline{\psi}i\slashed{\partial}\psi-\overline{\psi}m\psi-gt^a\overline{\psi}\gamma^{\mu}\psi A_{\mu}^a.
\end{equation}

\noindent By inspecting both the gluon kinetic term and the fermion mass term (bearing in mind that $[m]=1$) one can see that:

\begin{equation}
[\psi]=\frac{d-1}{2}, \qquad [A^\mu]=\frac{d-2}{2}.
\end{equation}

\noindent Thus the interaction term gives:

\begin{equation}
[g]=\frac{4-d}{2}.
\end{equation}

\noindent To use $g$ as an expansion parameter it must be massless. Hence the following transformation must be taken:

\begin{equation}
g \rightarrow \mu^\frac{4-d}{2}g.
\end{equation}

\noindent Given that the chosen value for $\mu$ is arbitrary, one would hope that the final value for any observable we calculate would be independent of it. This will be discussed further in section \ref{sec: quark coeff func}.

Putting these ingredients together one finds the $d$-dimensional version of equation (\ref{eq: 4DSigma}) to be \cite{florian,dissertori,Luisoni_2015} 

\begin{equation}\label{eq: dDSigma}
\begin{split}
\sigma_R^{(d)}=\sigma_0^{(d)}\frac{\alpha_S}{2\pi}C_F & \frac{1}{\Gamma(d-2)} \left(\frac{4\pi\mu^2}{Q^2}\right)^{\frac{4-d}{2}} \\ & \times \int^1_0\dd{x_1}\int^1_{1-x_1}\dd{x_2}\frac{x_1^2+x_2^2+\frac{d-4}{2}x_3^2}{(1-x_1)^\frac{6-d}{2}(1-x_2)^\frac{6-d}{2}(1-x_3)^\frac{4-d}{2}},
\end{split}
\end{equation}

\noindent where $\sigma_0^{(d)}$ is the Born level cross section in $d$-dimensions and can be found in \cite{dissertori}. Conservation of energy is expressed as $E_1+E_2+E_3 = Q$. Given that the energies of the partons must be less than $Q$ the $x_i$ variables can only vary between 0 and 1. Conservation of energy can be written in terms of these variables as $x_1+x_2+x_3=2$. This equation can be used to impose a more stringent limit on $x_2$; the minimum value of $x_2$ is achieved when $x_3$ is at its maximum, 1. This gives $x_2 = 1-x_1$ which becomes the true minimum of $x_2$, hence the limits of integration in equation (\ref{eq: dDSigma}). Performing this integration gives:

\begin{equation}
    \sigma_R^{(d)}=\sigma_0^{(d)}\frac{\alpha_S}{2\pi}C_F \frac{1}{\Gamma(d-2)} \left(\frac{4\pi\mu^2}{Q^2}\right)^{\frac{4-d}{2}} \frac{\Gamma\left(\frac{d}{2}-2\right)^2\Gamma\left(\frac{d}{2}-1\right)}{2\Gamma\left(\frac{3d}{2}-3\right)}(d-3)(d^2-4d+8).
\end{equation}

\noindent It is illuminative to now make the substitution $d=4-2\epsilon$. In the limit $d\rightarrow 4$, $\epsilon$ will be a small number and so it is possible to perform a  Laurent series expansion about $\epsilon=0$:

\begin{equation}\label{eq: inc with poles}
    \sigma_R^{(\epsilon)}=\sigma_0^{(\epsilon)}\frac{\alpha_S}{2\pi}C_F \frac{1}{\Gamma(1-\epsilon)} \left(\frac{4\pi\mu^2}{Q^2}\right)^{\epsilon} \left(\frac{2}{\epsilon^2} + \frac{3}{\epsilon} + \frac{19}{2} - \pi^2 + \mathcal{O}(\epsilon)\right).
\end{equation}

\noindent This expansion makes explicit where the infrared singularities have been hiding; the first two terms are poles at $\epsilon=0$. 

Next, consider the virtual correction. As written in equation (\ref{eq: nlo amplitudes}), this correction is built from the product of Born and virtual diagrams. In order to evaluate the virtual diagram the gluon and fermion propagators must be integrated over the gluon momentum. This integral again contains infrared divergences and dimensional regularisation can be used to evaluate it. Explicitly it is written as:

\begin{equation} \label{eq: inc virtual}
    \sigma_V^{(d)} \propto \int \frac{\dd[d]{k}}{(2\pi)^d}\frac{\bar{u}(p_2)\gamma^\nu(\slashed{k}+\slashed{p}_2)\gamma^\mu(\slashed{k}-\slashed{p}_1)\gamma_\nu\nu(p_1)}{[(k+p_2)^2+i\epsilon][(k-p_1)^2+i\epsilon][k^2+i\epsilon]}.
\end{equation}

\noindent Notice the slight difference here between the virtual and real terms. In the real terms the poles are generated as part of the cross-section integration, whereas here it is part of the matrix element. Nevertheless, at cross-section level one finds the virtual contribution to be:

\begin{equation}\label{eq: integrated virt}
    \sigma_V^{(\epsilon)}=\sigma_0^{(\epsilon)}\frac{\alpha_S}{2\pi}C_F \frac{1}{\Gamma(1-\epsilon)} \left(\frac{4\pi\mu^2}{Q^2}\right)^{\epsilon} \left(-\frac{2}{\epsilon^2} - \frac{3}{\epsilon} - 8 + \pi^2 + \mathcal{O}(\epsilon)\right).
\end{equation}

Seemingly by magic this integration, which superficially looks so different to the real integral, produces the exact same poles as equation (\ref{eq: inc with poles}). Of course this is not magic at all but a direct consequence of the KLN theorem. The full cross section at NLO is now written as:

\begin{align}
    \sigma^{(\epsilon)} = & \sigma_0^{(\epsilon)} + \sigma_R^{(\epsilon)}+\sigma_V^{(\epsilon)}, \\
     = & \sigma_0^{(\epsilon)} + \sigma_0^{(\epsilon)}\frac{\alpha_S}{2\pi}C_F \frac{1}{\Gamma(1-\epsilon)} \left(\frac{4\pi\mu^2}{Q^2}\right)^{\epsilon} 
    \left( \frac{3}{2} + \mathcal{O}(\epsilon)\right), \label{eq: tot inc epsi} \\
    \sigma = & \sigma_0\left(1+C_F\frac{3\alpha_s}{4\pi}\right),
\end{align}

\noindent where in the last line the limit $\epsilon\rightarrow 0$ has been taken, bringing us back to 4-dimensions. This kills the $\mathcal{O}(\epsilon)$ terms and one can see that we are left with a finite result. Hence we have seen how a finite NLO cross section can be constructed from singular matrix elements. 

All that is left is to extend this parton-level result to hadron level. In the case of such a simple observable the extension is trivial; it is exactly the same. This is because of unitarity: the production of quarks is the necessary and sufficient condition for the production of hadrons, thus the probability of one is the probability of the other. In the following section we will consider a more interesting and subtle observable.

\section{Hadron fractional energy distribution} \label{sec: hadr frac energy}

This section will focus on the calculation of $\dv{\sigma}{z}$ where $z$ is the fractional energy of a hadron produced in the $e^+e^-$ collision. The parton-level distribution can be calculated by introducing delta functions into equation (\ref{eq: dDSigma}):

\begin{equation}\label{eq: dDSigma z}
\begin{split}
    \dv{\sigma_R^{(d)}}{\hat{z}}=&\sigma_0^{(d)}\frac{\alpha_S}{2\pi}C_F \frac{1}{\Gamma(d-2)} \left(\frac{4\pi\mu^2}{Q^2}\right)^{\frac{4-d}{2}}\int^1_0\dd{x_1}\int^1_{1-x_1}\dd{x_2} \\ 
        & \times \frac{x_1^2+x_2^2+\frac{d-4}{2}x_3^2}{(1-x_1)^\frac{6-d}{2}(1-x_2)^\frac{6-d}{2}(1-x_3)^\frac{4-d}{2}}\sum_i\delta(x_i-\hat{z}).
\end{split}
\end{equation}

\noindent The sum over $i$ represents summing over the fractional energies of the three final state partons. 

\subsection{Quark and antiquark coefficient function}
\label{sec: quark coeff func}
First let us focus on the quark, meaning the case $i=1$. Note that equation (\ref{eq: dDSigma z}) is symmetric about $x_1 \leftrightarrow x_2$ and so gives the same answer for the antiquark too. The $x_3$ dependence can be removed using conservation of energy: $x_3=2-x_1-x_2$. After performing the integration, we obtain an answer that can be factorised into the form of $(1-\hat{z})^{-\epsilon-1} \ f(\hat{z},\epsilon)$. At this point, it would be tempting to perform a Laurent expansion in $\epsilon$ as we did for the inclusive total cross section. However, in doing this we risk losing our regularisation about $\hat{z}\rightarrow1$ as:

\begin{equation}
(1-\hat{z})^{-\epsilon-1}= \frac{1}{1-\hat{z}}-\epsilon \frac{  \log (1-\hat{z})}{(1-\hat{z})}+O\left(\epsilon ^2\right).
\end{equation}

\noindent The order epsilon term has been included here as $f(\hat{z},\epsilon)$ contains order $\epsilon^{-1}$ terms.

From the KLN theorem we expect that the soft divergences at $\hat{z}=1$ will cancel with terms from the virtual diagrams. These virtual terms will only contribute at the point where $\hat{z}=1$ as this is effectively the point where there is no real emission and so it matches the final state of the virtual diagram. Hence it is helpful to introduce a notation known as ``the plus prescription'', defined as

\begin{equation}\label{eq: plus}
\int_0^1 h(\hat{z}) \left[g(\hat{z})\right]_+\dd{\hat{z}}=\int^1_0g(\hat{z})[h(\hat{z})-h(1)]\dd{\hat{z}},
\end{equation}

\noindent where $g(\hat{z})$ is any function and $h(\hat{z})$ is regular as $\hat{z} \rightarrow 1$ . The significance of this prescription is that even if $g(\hat{z})$ is singular at $\hat{z}=1$, $[g(\hat{z})]_+$ will not be as

\begin{equation}
    \lim_{\hat{z} \to 1} h(\hat{z})-h(1)=0.  
\end{equation}

\noindent One can then show:

\begin{align}
(1-\hat{z})^{-\epsilon-1} & = \left[ (1-\hat{z})^{-\epsilon-1}\right]_+ -\frac{1}{\epsilon}\delta(1-\hat{z}) \\
&=-\frac{1}{\epsilon}\delta(1-\hat{z})+\left[\frac{1}{1-\hat{z}}\right]_+-\epsilon \left[ \frac{\log (1-\hat{z})}{(1-\hat{z})}\right]_+ +O\left(\epsilon ^2\right).
\end{align}

\noindent Thus we are able to expand $(1-\hat{z})^{-\epsilon-1}$, and so the real emission cross section, in a way that preserves regularisation. 

Having done this, the next step is to combine it with the Born cross section $\dv{\sigma_0}{\hat{z}}$ and virtual correction $\dv{\sigma_V}{\hat{z}}$. As discussed earlier these terms should only contribute at the point $\hat{z}=1$, as this is effectively the point without gluon emission. Of course it must also be that when integrated over $\hat{z}$ the terms equal $\sigma_0$ and $\sigma_V$ respectively. Hence the two terms end up being:

\begin{equation}\label{eq: virtual diff}
    \dv{\sigma_B}{\hat{z}}=\delta(1-\hat{z})\sigma_0, \quad \dv{\sigma_V}{\hat{z}}=\delta(1-\hat{z})\sigma_V,
\end{equation}

\noindent where $\sigma_V$ is simply the result we calculated in equation (\ref{eq: inc virtual}). The full cross section accurate to NLO is $\dv{\sigma}{\hat{z}} = \dv{\sigma_0}{\hat{z}}+\dv{\sigma_V}{\hat{z}} + \dv{\sigma_R}{\hat{z}}$. Putting equations (\ref{eq: virtual diff}) together with the integrated and expanded $\dv{\sigma_R}{\hat{z}}$ gives:

\begin{equation}\label{eq: partonDiffXS}
\begin{split}
    \dv{\sigma}{\hat{z}}= &  
        \sigma_0^{(\epsilon)}\delta(1-\hat{z}) +
        \sigma_0^{(\epsilon)}
        \frac{\alpha_S}{2\pi}
        C_F 
        \frac{1}{\Gamma(1-\epsilon)} 
        \left(\frac{4\pi\mu^2}{Q^2}\right)^{\epsilon}
     \\ &\times \left[ -\frac{1}{\epsilon}\left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+ + \left(1+\hat{z}^2\right) \left[\frac{\log(1-\hat{z})}{1-\hat{z}}\right]_+ -\frac{3}{2} \left[\frac{1}{1-\hat{z}}\right]_+ \right. \\ & \left. \hspace{1cm} +2\frac{1+\hat{z}^2}{1-\hat{z}}\log (\hat{z}) +\frac{3}{2}(1-\hat{z}) +\left(\frac{2\pi^2}{3}-\frac{9}{2}\right) \delta (1-\hat{z}) +\mathcal{O}(\epsilon) \right].
\end{split}
\end{equation}

\noindent Unlike with the inclusive total cross section, for this observable one can see there remains a $\frac{1}{\epsilon}$ pole and so this result is not finite in 4-dimensional space. However, this is not a violation of the KLN theorem but rather an example of an observable that is not collinear safe. A collinear emission off the quark will carry away some of its energy and so $\hat{z}$ will change, meaning $\dv{\sigma}{\hat{z}}$ is sensitive to collinear emissions. Note though that an infinitely soft emission will not change $\hat{z}$ and so the observable is infrared safe, hence there are no $\frac{1}{\epsilon^2}$ poles in equation (\ref{eq: partonDiffXS}).

IRC safety can be achieved by calculating the cross-section distribution for the fractional energy of a hadron, $z$, rather than a quark. If a hadron radiates a parton that parton will remain inside the hadron so long as it is collinear, hence $z$ is insensitive to collinear emissions. The hadronic cross section is calculated using the QCD factorisation theorem. Following this theorem we construct the calculation as the partonic cross section convoluted with a fragmentation function, which we write as $D_i^h$. This function describes the physics occurring at scales lower than the partonic scattering. At leading order one can think of it as the probability for a parton of flavour $i$ with fractional energy $\hat{z}$ to become a hadron with energy $z$. At higher orders though this picture no longer holds because, as we will see, $D_i^q$ is no longer guaranteed to be a positive definite.

\begin{figure}
    \centering
    \input{input/ee2hadr/ff}
    \caption{Graphical representation of the process $e^+e^-\to hX$, where $h$ represents a single hadron of flavour $h$ and $X$ represents all other hadrons in the event. The blue blob represents the partonic matrix element for $e^+e^-\to q\bar{q}g$, given by the diagrams in figure \ref{fig: feyn ee2quarks}. The red blobs represent the non-perturbative hadronisation of these partons. In this diagram we have focussed on the case where $h$ originates from the quark, which is modelled using $D^h_q$. Similar diagrams can be drawn for when $h$ originates from the gluon or antiquark.}
    \label{fig: frag func}
\end{figure}

In equation (\ref{eq: partonDiffXS}), $\hat{z}$ is defined as the fractional energy of the quark relative to the total momentum of the scattering, $Q$. To ensure the fragmentation function remains independent of the scattering process, we write it as being dependent on $\frac{z}{\hat{z}}$ so that the $Q$ dependence drops out. In general it will also depend on the scale $\mu$. Consider the process $e^+e^-\to hX$, where $hX$ represents a final state consisting of a single hadron of flavour $h$ alongside other hadrons. We can now use our calculation of $\dv{\sigma}{\hat{z}}$ to calculate the energy spectrum of $h$ \cite{Tanabashi:2018oca,Metz:2016swz}:

\begin{align}\label{eq: frag conv}
    \dv{\sigma}{z}=&\sum_i\int_{z}^1\frac{\dd{\hat{z}}}{\hat{z}} D^h_{i}\left(\frac{z}{\hat{z}},\mu\right)\dv{\sigma}{\hat{z}}\left(\hat{z},\mu\right).
\end{align}

\noindent The sum over $i$ represents summing over all final state partons that could hadronise to produce $h$, for our purposes $i\in \{q,\bar{q},g\}$. A diagrammatic representation of this equation in the case of $i=q$ is shown in figure \ref{fig: frag func}. For the rest of this subsection we will only look at the quark fragmentation function into any hadron and so occasionally suppress the $i$ and $h$ indices. 

Hadrons depend on non-perturbative physics and so fragmentation functions cannot be calculated from first principles (using currently available techniques). Note though that such a calculation would again not be IRC safe as it depends on the energy of the specific parton being hadronised. This means that if it were possible to calculate the fragmentation function from first principles in $d$-dimensions we would find it contains a $\frac{1}{\epsilon}$ pole. However, from experiment it is evident that $\dv{\sigma}{z}$ is not infinite. Hence, the pole in $D$ must be of the form $+\frac{1}{\epsilon}\left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+$ so that it cancels the one in equation (\ref{eq: partonDiffXS}). 

Furthermore, a calculation of $D$ in $d$-dimensions would also introduce a dependence on $\mu$. Remember $\mu$ is arbitrary and unphysical and so the final cross section cannot depend on it. Hence the dependence on $\mu$ in equation (\ref{eq: partonDiffXS}) must cancel with that in the fragmentation function. To fully understand this dependence we must expand the pre-factors in equation (\ref{eq: partonDiffXS}) as :

\begin{equation}
    \frac{1}{\Gamma(1-\epsilon)} \left(\frac{4\pi\mu^2}{Q^2}\right)^{\epsilon} = 1 + \epsilon \log\left(\frac{\mu^2}{Q^2}\right) + \epsilon\log\left(4 \pi\right) - \epsilon\gamma_E +\mathcal{O}(\epsilon^2),
\end{equation}

\noindent where $\gamma_E$ is Euler's constant. The $\mathcal{O}(\epsilon)$ terms here will combine with the epsilon pole in equation (\ref{eq: partonDiffXS}) to give:

\begin{equation}\label{eq: mu and pole dep}
\begin{split}
    \dv{\sigma}{\hat{z}} = &
        \sigma_0^{(\epsilon)}\delta\left(1-\hat{z}\right) +
        \sigma_0^{(\epsilon)}
        \frac{\alpha_S}{2\pi}
        C_F \\ 
        &\times \left\{ 
        \left(
        -\frac{1}{\epsilon}-\log\frac{\mu^2}{Q^2} - \log4 \pi + \gamma_E 
        \right)\left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+ 
        +   
        R(\hat{z})+\mathcal{O}(\epsilon) 
        \right\}.
\end{split}
\end{equation}

\noindent Here $R(\hat{z})$ has been used to represent all the non-singular $\hat{z}$-dependent terms, which can be read off from equation (\ref{eq: partonDiffXS}) as :

\begin{equation}
\begin{split}
    R(\hat{z}) = & \left(1+\hat{z}^2\right) \left[\frac{\log(1-\hat{z})}{1-\hat{z}}\right]_+ -\frac{3}{2} \left[\frac{1}{1-\hat{z}}\right]_+ + 2\frac{1+\hat{z}^2}{1-\hat{z}}\log (\hat{z})  \\ &  +\frac{3}{2}(1-\hat{z}) +\left(\frac{2\pi^2}{3}-\frac{9}{2}\right) \delta (1-\hat{z}).
\end{split}
\end{equation} 

From equations (\ref{eq: frag conv}) and (\ref{eq: mu and pole dep}) it follows that, if $D$ is going to cancel the pole and $\mu$ dependence of $\dv{\sigma}{\hat{z}}$, the most general form it can take is:

\begin{equation}\label{eq: frag mu dep}
\begin{split}
    D_q^h\left(z, \mu\right) = 
    \int_z^1 & \frac{\dd{\hat{z}}}{\hat{z}}
    D_{\text{NS}}\left(\frac{z}{\hat{z}},  \mu_F\right)
    \left\{\delta(1-\hat{z}) \phantom{\log\frac{\mu^2}{\mu_F^2}} \right. \\ & \left. 
    -\frac{\alpha_S}{2\pi} C_F \left(
    -\frac{1}{\epsilon}\left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+
    -\log\frac{\mu^2}{\mu_F^2}\left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+
    +K(\hat{z})
    \right)
    \right\}.
\end{split}
\end{equation}

\noindent Several features of this equation need explaining. The first thing to note here is that in order to keep things dimensionally consistent we have had to introduce a new scale $\mu_F$, known as the factorisation scale. Unlike $\mu$ though, this scale has physical interpretation. Remember that the factorisation theorem dictates that high scale physics is described by the partonic cross section and everything else by the fragmentation function. However, exactly what we mean by `everything else' is of course dependent on where exactly we place this cut-off in scale. An all-orders calculation will remove dependence on this cut-off but this is not guaranteed for a fixed-order calculation such as ours. This is exactly what $\mu_F$ is: the scale at below which physics is described by the fragmentation function.

The next thing to discuss is what is meant by $D_{\text{NS}}$. From equation (\ref{eq: mu and pole dep}) we are only able to read off the pole and $\mu$ dependence of $D$. The rest of its dependence on $\hat{z}$ is unknown and so $D_{\text{NS}}$ is introduced to cover this. What we do know about this function is that:

\begin{short_enum}
    \item It must be non-singular (hence the `NS').
    \item It has no dependence on $\mu$.
    \item It should only be used in a calculation that is accurate to order $\alpha_S$.
\end{short_enum}

\noindent The final thing to note about equation (\ref{eq: frag mu dep}) is the introduction of $K(\hat{z})$. This represents a freedom we have in how $D_{\text{NS}}$ is defined. $K$ can be chosen to be any non-singular function of $\hat{z}$ and it can always be absorbed away by a re-definition of $D_{\text{NS}}$. Hence, $K$ does not contribute at order $\alpha_S$. In what follows we will use a $K$ that removes the $\log 4 \pi$ and $\gamma_E$ terms from our expressions:

\begin{equation}
    K = \log (4 \pi) - \gamma_E.
\end{equation}

\noindent This choice is known as the modified minimal subtraction scheme, $\overline{\text{MS}}$.

Substituting equations (\ref{eq: mu and pole dep}) and (\ref{eq: frag mu dep}) into (\ref{eq: frag conv}) and keeping only terms that are less than $\mathcal{O}(\alpha_S^2)$ gives:

\begin{equation} \label{eq: before coeff func ep}
    \dv{\sigma}{z} = \sigma_0^{(\epsilon)} \int_{z}^1\frac{\dd{\hat{z}}}{\hat{z}}D_{\text{NS}}\left(\frac{z}{\hat{z}},\mu_F\right) \left\{ \delta(1-\hat{z}) - \frac{\alpha_S}{2\pi} C_F  \left( \left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+\log\frac{\mu_F^2}{Q^2} + R(\hat{z}) + \mathcal{O}(\epsilon)  \right) \right\}.
\end{equation}

\noindent Notice how there is no pole or $\mu$ dependence in this equation, thanks to how equation (\ref{eq: frag mu dep}) was constructed. Thus this equation presents (at last) a fully regularised and physically sensible result for the differential cross section. The only remaining step is to return to 4-dimensions by taking the limit $\epsilon\rightarrow 0$:

\begin{equation} \label{eq: before coeff func}
    \dv{\sigma}{z} = \sigma_0 \int_{z}^1\frac{\dd{\hat{z}}}{\hat{z}}D_{\text{NS}}\left(\frac{z}{\hat{z}},\mu_F\right) \left\{ \delta(1-\hat{z}) - \frac{\alpha_S}{2\pi} C_F  \left( \left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+\log\frac{\mu_F^2}{Q^2} + R(\hat{z}) \right) \right\}.
\end{equation}

It is common to absorb the terms in the curly braces into what is called the quark coefficient function, $C_q$. One can also obtain coefficient functions for the gluon and antiquark such that the full hadronic cross section can be written as:

\begin{equation} \label{eq: frag phys convolution}
    \dv{\sigma}{z} = \sum_i \int_{z}^1\frac{\dd{\hat{z}}}{\hat{z}}D_{\text{NS}}\left(\frac{z}{\hat{z}},\mu_F\right) C_i(\hat{z}, Q, \mu_F),
\end{equation}

\noindent where $i\in\{q\bar{q},g\}$. The quark coefficient function written out explicitly is \cite{NASON1994473}:

\begin{align}\label{eq: quark coeff}
    C_q(\hat{z}, Q, \mu_F) = & \sigma_0\left[\delta(1-\hat{z}) +  \frac{\alpha_S}{2\pi}
            C_F f (\hat{z}, Q, \mu_F) \right] \\
\begin{split}    
    f (\hat{z}, Q, \mu_F) = & \left(1+\hat{z}^2\right) \left[\frac{\log(1-\hat{z})}{1-\hat{z}}\right]_+ -\frac{3}{2} \left[\frac{1}{1-\hat{z}}\right]_+ +2\frac{1+\hat{z}^2}{1-\hat{z}}\log (\hat{z}) +\frac{3}{2}(1-\hat{z}) \\ 
    & +\left(\frac{2\pi^2}{3}-\frac{9}{2}\right) \delta (1-\hat{z}) -\log\left(\frac{\mu_F^2}{Q^2}\right) \left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+.
\end{split}
\end{align}

The only remaining ingredient required to compare this expression to experimental data is to know what value to use for $\mu_F$. Ideally $\mu_F$ would be set to the value at which non-perturbative physics no longer contributes. Of course though in reality there is no hard cut-off beyond which the physics is purely perturbative. Instead, it is a continuous spectrum. The best we can do is make a choice guided by physical insight. One can also show that a matrix element of order $n$ in $\alpha_S$ will contain terms proportional to $\alpha^n\left(\log\frac{\mu_F^{2}}{Q^{2}}\right)^n$. Hence a choice of $\mu_F \sim Q$ is important to ensure higher order terms are small and so our perturbative series converges.  After choosing such a value, one can confirm that the final cross section does not heavily depend on the precise value of $\mu_F$ by comparing it with values at $2\mu_F$ and $\mu_F/2$. 

\subsection{Gluon coefficient function}
The gluon coefficient function, $C_g$, is used to obtain the cross section in which it is the gluon that hadronises. It can be calculated using equation (\ref{eq: dDSigma z}), substituting for $x_2$ using $x_2=2-x_1-x_3$ and setting $i=3$. Following the same procedure as in the previous section one obtains the following result:

\begin{equation}\label{eq: dsigdz gluon}
\begin{split}
\dv{\sigma_R}{\hat{z}}= \sigma_0^{(\epsilon)}
        \frac{\alpha_S}{2\pi}
        C_F 
        \frac{1}{\Gamma(1-\epsilon)}& 2\frac{
   1+(1-\hat{z})^2}{\hat{z}} \left[-\frac{1}{\epsilon} + \log (1-\hat{z})+2 \log
   (\hat{z})-\log \left(\frac{\mu
   ^2}{Q^2}\right)\right].
\end{split}
\end{equation}

\noindent Note in this case there is no virtual correction because if the gluon recombines with the quark or antiquark to form a loop it will be unable to initiate a hadron. This makes sense given that, although we have used the NLO matrix element, we are calculating the leading order term in $C_g$ (there is no gluon in the Born-level to form a hadron). Removing terms that cancel with the fragmentation function and setting $\epsilon=0$ gives

\begin{equation}\label{eq: gluon coeff}
C_g(\hat{z},\mu_F)= \sigma_0 \frac{\alpha_S}{2\pi} C_F\frac{ 1+(1-\hat{z})^2}{\hat{z}}2\left[\log (1-\hat{z})+2 \log
   (\hat{z})-\log \left(\frac{\mu_F
   ^2}{Q^2}\right)\right].
\end{equation}    

\subsection{DGLAP equation} \label{sec: dglap}

As has been discussed, $\mu_F$ is the scale below which physics is described by the fragmentation functions. Of course the transition from perturbative to non-perturbative physics will not happen at some sharp cut-off. Instead it will be a gradual and continuous transition. Hence, for relatively high scales (somewhere between $\mu_F$ and $\Lambda_{QCD}$), it is possible to describe some features of fragmentation functions using perturbative physics.

To see this, consider again equation (\ref{eq: frag mu dep}). Remember that $D$ is defined as whatever is left from the hadronic calculation after you factor out the partonic matrix element. In principle it should only depend on parameters found in the QCD Lagrangian (plus of course kinematic variables). This means it will have no dependence on the factorisation scale $\mu_F$, which does not come from the Lagrangian but rather is an artefact of perturbative calculations. Hence we can write the following equation:

\begin{equation}
    \dv{}{\mu_F}D\left(z,\mu\right)=0.
\end{equation}

\noindent Hence, taking the derivative of equation (\ref{eq: frag mu dep}) with respect to $\mu_F$ gives:

\begin{equation} \label{eq: bare quark dglap}
    \mu_F\dv{}{\mu_F}D_{\text{NS}}\left({z},\mu_F\right) = \frac{\alpha_S}{\pi} C_F \int_{z}^1\frac{\dd{\hat{z}}}{\hat{z}} \left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+ D_{\text{NS}}\left(\frac{z}{\hat{z}},\mu_F\right).
\end{equation}

\noindent This is analogous to the renormalisation group equation we encountered in section \ref{sec: singularities}. Note that this equation is only accurate to $\mathcal{O}(\alpha_S)$. In order to derive it we have used the fact that, just as with partonic matrix element, the dependence on $\mu_F$ will only occur at $\mathcal{O}(\alpha_S)$. From equation (\ref{eq: bare quark dglap}) we can now read-off the scale dependence of the fragmentation functions. Hence if the fragmentation function is experimentally measured at a certain scale, it is possible to predict its value for all other scales. 

In practice, one has to use a different fragmentation function for each flavour of quark as well as a separate one for gluons. Furthermore there will be contributions not just from the $q\rightarrow qg$ splitting but also from $g\rightarrow gg$ and $g \rightarrow qq$. All this information can be succinctly encapsulated in the following equation:

\begin{equation}\label{eq: dglap frag}
    \mu_F\dv{}{\mu_F}
    \begin{pmatrix}
        D_i(z, \mu_F) \\ D_g(z, \mu_F)
    \end{pmatrix}
     = \sum_j\frac{\alpha_S}{\pi}\int_x^1\frac{\dd{\hat{z}}}{\hat{z}}
    \begin{pmatrix}
        P_{q_iq_j}\left(\hat{z}\right) & P_{gq_i}\left(\hat{z}\right) \\
        P_{q_jg}\left(\hat{z}\right) & P_{gg}\left(\hat{z}\right)
    \end{pmatrix}
    \begin{pmatrix}
        D_j(\frac{z}{\hat{z}}, \mu_F) \\ D_g(\frac{z}{\hat{z}}, \mu_F)
    \end{pmatrix}
    .
\end{equation}

\noindent Here, the $h$ index has been dropped. The $i$ and $j$ indices represent summing over quark flavours. The $P$ functions are known as the Altarelli-Parisi splitting functions \cite{Altarelli:1977zs}. The functions can be written explicitly as
\begin{align}
    \label{eq: p_qq} P_{qq}(\hat{z}) &= C_F\left[\frac{1+\hat{z}^2}{1-\hat{z}}\right]_+,\\
    \label{eq: p_qg} P_{qg}(\hat{z}) &= C_F\frac{1+(1-\hat{z})^2}{\hat{z}},\\
                     P_{gq}(\hat{z}) &= T_R\left(\hat{z}^2+(1-\hat{z})^2\right),\\
    \label{eq: p_gg} P_{gg}(\hat{z}) &= 2C_A\left[\frac{\hat{z}}{[1-\hat{z}]_+}+\frac{1-\hat{z}}{\hat{z}}+\hat{z}(1-\hat{z})\right]+\frac{\beta_0}{2}\delta(1-\hat{z}),
\end{align}

\noindent where $\beta_0 = \frac{11}{3}C_A - \frac{4}{3}T_F$. Notice, equation (\ref{eq: p_qq}) and (\ref{eq: p_qg}) have already been derived in equation (\ref{eq: quark coeff}) and (\ref{eq: gluon coeff}) respectively. The other splitting functions could be derived by considering processes with a $g\rightarrow qq$ and $g\rightarrow gg$ splitting. Crucially these functions are universal, one finds the same result no matter what the process is (discussed further in section \ref{sec: collinear limit}). This is important as it preserves the universality of fragmentation functions.

If one considers initial rather than final state radiation, the scale dependence of parton distribution functions, $f$, can be found. Remarkably it comes out to be the same as in equation (\ref{eq: dglap frag}), but swapping out $D \rightarrow f$. These are known as the DGLAP equations, after Dokshitzer \cite{Dokshitzer:1977sg},  Gribov and Lipatov \cite{Gribov:1972ri} and Altarelli and Parisi \cite{Altarelli:1977zs}. In this work the splitting functions have been presented at LO, but by considering higher order processes the functions have been calculated at NLO \cite{Curci:1980uw} and NNLO \cite{Zijlstra:1992qd,MOCH2000853,VERMASEREN20053}. The agreement of these predictions with the measured scale dependence of PDFs \cite{Abramowicz:2015mha} allow for confidence in QCD Factorisation and the other principles that go into deriving an equation like (\ref{eq: dglap frag}).

\section{Jet fractional energy}
In section \ref{sec: hadr frac energy} an IRC safe observable was constructed by promoting the parton-level cross section to hadron level. However, this is not the only safe observable one could construct. The hadrons produced in particle collisions will not be uniformly spread but more likely clustered into narrow energetic bands. These narrow bands are referred to as jets and are a helpful object to consider when analysing proton-proton collisions. Notice how jet energy, $z_j$ is a collinear safe observable: a collinear emission will remain within the jet and so not affect the total energy. Hence a calculation of $\dv{\sigma}{z_j}$ should give a finite result.

Before performing this calculation, it is necessary to define what exactly a jet is. Exactly how energetic and collimated the band of hadrons needs to be to be considered as a jet and not just a cluster of independent hadrons is an arbitrary choice and must be specified in a “jet definition”. This jet definition will also describe how we define the total momentum of a collection of partons, technically referred to as recombination. Several factors go into determining what makes a good definition. It should be easy to implement in both experimental and theoretical settings and be well defined at detector, hadron and particle level.

A common definition is known as the Cambridge-Aachen algorithm \cite{Dokshitzer:1997in,Salam:2009jx}. It introduces the following distance measure between two final state particles $i$ and $j$:

\begin{equation}
d_{ij}=2(1-\cos\theta_{ij}),
\end{equation}

\noindent where $\theta_{ij}$ is the angle between the two particles. In the case of $e^+e^-\rightarrow q\bar{q}$ the algorithm dictates that you find the smallest value for $d_{ij}$ across all values for $i$ and $j$. One must then compare it to a new parameter $R^2$, where $R$ is known as the jet radius and used to define how narrow we wish our jets to be. If this minimum $d_{ij}$ is below $R^2$ then we recombine $i$ and $j$ into a single particle and repeat the previous steps. In the case where this value for $d_{ij}$ is greater than $R^2$ then $i$ is promoted to the status of jet and we start again. The algorithm continues repeating until only jets are left.

The purpose of this section is not to give an explicit calculation of $\dv{\sigma}{z_j}$ but only to introduce the concept of jets. Proton-proton collisions do not result in a uniform spread of hadrons across the detector. Instead the hadrons are typically focussed into particular areas and so jets allow for a natural description of events. Jets are also well defined at both perturbative and hadron level. Hence they prove very useful and will be referred to later.

% section e+e-2hadrons (end)