%!TEX options = --shell-escape
\documentclass[12pt,PhD,twoside]{muthesis}
% The regulations say that 12pt should be used
% Change the MSc option to MPhil, MRes or PhD if appropriate

\usepackage[table]{xcolor}
\usepackage{verbatim}
\usepackage{graphicx}
\usepackage{url} % typeset URL's reasonably
\usepackage{listings}
\usepackage{enumitem}
\usepackage{placeins}

% Nicer link colours. 
% Taken from https://tex.stackexchange.com/questions/525261/better-default-colors-for-hyperref-links/525297#525297
% Can either colour the text or boxes
\usepackage[colorlinks]{hyperref}
\def\tmp#1#2#3{%
  \definecolor{Hy#1color}{#2}{#3}%
  \hypersetup{#1color=Hy#1color}}
\tmp{link}{HTML}{800006}
\tmp{cite}{HTML}{2E7E2A}
\tmp{file}{HTML}{131877}
\tmp{url} {HTML}{8A0087}
\tmp{menu}{HTML}{727500}
\tmp{run} {HTML}{137776}
\def\tmp#1#2{%
  \colorlet{Hy#1bordercolor}{Hy#1color#2}%
  \hypersetup{#1bordercolor=Hy#1bordercolor}}
\tmp{link}{!60!white}
\tmp{cite}{!60!white}
\tmp{file}{!60!white}
\tmp{url} {!60!white}
\tmp{menu}{!60!white}
\tmp{run} {!60!white}

% \usepackage{pslatex} % Use Postscript fonts

% Uncomment the next line if you want subsubsections to be numbered
% \setcounter{secnumdepth}{3}
% Uncomment the next line if you want subsubsections to be appear in
% the table of contents
%\setcounter{tocdepth}{3}

% Uncomment the following lines if you want to include the date as a
% header in draft versions
% \usepackage{fancyhdr}
% \pagestyle{fancy}
% \lhead{}  % left head
% \chead{Draft: \today} % centre head
% \lfoot{}
% \cfoot{\thepage}
% \rfoot{}

% Kiran's stuff
\usepackage{siunitx}
\usepackage{setspace}
\usepackage{physics}
\usepackage{slashed}
\usepackage{dsfont}
\usepackage{mathtools}
\let\MakeUppercase\relax
\usepackage[scaled]{helvet}
% \renewcommand*\rmdefault{dayrom}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{bm}
\usepackage{cleveref}
\usepackage{pgfplots}
\usepackage{multicol}
\usepackage{subcaption}
\usepackage{cite}

\usepackage{listings}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolor}{rgb}{0.95,0.95,0.92}
\definecolor{tabgrey}{rgb}{0.57,0.57,0.57}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolor},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=4
}
\lstset{
    literate={~} {$\sim$}{1},
    literate={|} {{{\color{tabgrey}|}}}{1},
    upquote=true,
    style=mystyle
}

\setlength {\marginparwidth }{3.5cm}
\usepackage{todonotes}
\reversemarginpar

\newenvironment{short_enum}{
\singlespacing
\begin{enumerate}
  \setlength{\itemsep}{8pt}
  \setlength{\parskip}{1pt}
  \setlength{\parsep}{1pt}
}{\end{enumerate}}

\newenvironment{short_enuma}{
\singlespacing
\begin{enumerate}[label=\alph*)]
  \setlength{\itemsep}{8pt}
  \setlength{\parskip}{1pt}
  \setlength{\parsep}{1pt}
}{\end{enumerate}}


\usepackage{tikz-feynman, contour}
\usetikzlibrary{shapes.geometric, arrows}
\usepgfplotslibrary{groupplots,dateplot}
\input{input/ext_tikz}
\pgfplotsset{compat=newest}
\newcommand{\Herwig}{\textsf{Herwig}}
\newcommand{\herwig}{\textsf{Herwig}}
\newcommand{\hwg}{\textsf{Herwig}}
\newcommand{\singphas}{\textsf{Singular\-Phasespace}}
\newcommand{\POWHEG}{\texttt{POWHEG}}
\newcommand{\MCatNLO}{\text{MC@NLO}}
\newcommand{\GeV}{\text{ GeV}}
\newcommand{\sunny}{\(\text{SU}(N)\)}
\newcommand{\psibar}{\bar{\psi}}
\newcommand\OpenLoops{\textsc{\small OpenLoops}}
\newcommand\HELAS{\textsc{\small HELAS}}
\newcommand{\HelRec}{\texttt{HelicityRecycler}}
\newcommand{\pT}{p_\text{T}}

\newcommand{\madnlo}{\textsc{MadGraph5\_a\-MC@NLO}}
\newcommand{\mg}{\textsc{MG5aMC}}
\newcommand{\mgnlo}{MG5aMC}
\newcommand{\aloha}{\textsc{Aloha}}

\definecolor{lightgrey}{HTML}{dedede}
\definecolor{lightergrey}{HTML}{f7f7f7}

\graphicspath{images}

\renewcommand\ttdefault{lmtt}

% Make math bold too
\makeatletter
\g@addto@macro\bfseries{\boldmath}
\makeatother

\begin{document}
% Uncomment the following lines to leave out list of figures, tables
% and copyright until final printing
%\figurespagefalse
%\tablespagefalse
%\copyrightfalse

% Uses boxes instead of coloured text
\makeatletter\def\@pdfborder{1 1 0.3} \def\HyColor@UseColor#1{}\makeatother

\title{Improving Monte-Carlo simulations\\
	   of perturbative QCD Physics}
\author{Kiran Ostrolenk}
\stuid{8306213}
\principaladviser{Prof. Michael Seymour}

\beforeabstract

\prefacesection{Abstract}
\input{abstract}

\afterabstract

\prefacesection{Acknowledgements}
I start by thanking my supervisor Mike for his continued insight and patience. Back in 2015 he took a chance on me, a simple-minded $3^\text{rd}$ year undergrad, for a summer project. So began a 5-year collaboration, culminating in this thesis. I also thank Olivier for his brief but active and useful co-supervision. I would also like to thank my peers in the HEP theory office for their stimulating conversations and high-quality banter. This includes Graeme, Sotirios, Matthew, Baptiste and Jack. This gratitude also extends to the wider HEP Manchester community, who have done so much to help keep me sane. This includes Nico, Jacob, Pablo and all the rest! I also want to particularly thank Vale, whose support and lovely company has been invaluable over the last year. Finally, I thank my family and the OGs back in London; I never would have made it this far without them.

\afterpreface

% TODO:
% Explain IRC safety
% Introductions plus per chapter conclusions

% The chapters
\input{chapters}

% Bibliography
\bibliography{refs}
\bibliographystyle{ieeetr}

% Appendices start here

% \appendix
% \include{appendix1}

\end{document}
