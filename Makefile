CHAPS = \
chapter1 chapter2 chapter3 chapter4 chapter5 chapter6

all : report.tex
	lualatex report.tex
	lualatex report.tex
	evince report.pdf&

sepchaps : $(CHAPS)

chapter% : report.tex
	mkdir -p tikzfigures
	sed -i 's,\\input{chapters},\\input{chaps/$@},g' report.tex
	sed -i 's,\\input{input/ext_tikz},%\\input{input/ext_tikz},g' report.tex
	cat report.tex
	lualatex report.tex
	lualatex report.tex
	lualatex report.tex
	sed -i 's,\\input{chaps/$@},\\input{chapters},g' report.tex
	sed -i 's,%\\input{input/ext_tikz},\\input{input/ext_tikz},g' report.tex
	mv report.pdf $@.pdf

clean :
	rm *pdf
